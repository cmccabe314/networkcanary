/*
 * Copyright (c) 2013, the networkcanary authors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * networkcanary
 *
 * A simple program which monitors a set of hostnames over time.  It will
 * complain if any of the hosts can't be reached.
 * We try to connect to TCP port 22 on all the hosts, assuming that this will
 * provide a good proxy for network connectivity because of the three-way
 * handshake required by TCP.  (We could have used ping, aka ICMP, but sending
 * ICMP packets on Linux required root privileges in older kernels.)
 */

package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"strings"
	"time"
)

func runCheck(logChan chan<- string, start time.Time, hostname string) {
	// resolve hostname
	addr, err := net.ResolveIPAddr("ip4", hostname)
	duration := time.Since(start)
	if duration.Seconds() > *warnPeriodSecs {
		logChan <- fmt.Sprintf("%s ERROR: long time resolving addr %s " +
			" to IP %s: took %s\n", start.String(), addr,
			addr, duration.String())
	}
	if addr == nil {
		logChan <- fmt.Sprintf("%s ERROR: could not resolve hostname %s\n",
						start.String(), hostname)
		return
	}
	// open connection to socket
	start = time.Now()
	toDial := addr.String() + ":22"
	var conn net.Conn
	conn, err = net.Dial("tcp", toDial); if err != nil {
		logChan <- fmt.Sprintf("%s ERROR: Dial(tcp, %s) failed: %s\n",
				start.String(), toDial, err.Error())
		return;
	}
	conn.Close()
	duration = time.Since(start)
	if duration.Seconds() > *warnPeriodSecs {
		logChan <- fmt.Sprintf("%s ERROR: connecting to %s " +
			" took %s\n", addr, duration.String())
	}
}

func runCheckThread(logChan chan<- string, hostname string) {
	checkStart := time.Now()
	lastLog := checkStart
	for ;; {
		loopStart := time.Now()
		if (loopStart.Sub(lastLog.Add(
				time.Duration(*infoPeriodSecs) * time.Second)) > 0) {
			logChan <- fmt.Sprintf("%s INFO: have been pinging %s for %s\n",
					loopStart.String(), hostname, loopStart.Sub(checkStart))
			lastLog = loopStart
		}
		runCheck(logChan, loopStart, hostname)

		loopEnd := loopStart.Add(time.Duration(*checkPeriodSecs) *
			time.Second)
		toWait := loopEnd.Sub(loopStart)
		if (toWait > 0) {
			time.Sleep(toWait)
		}
	}
}

var hostnames = flag.String("hostnames", "",
	"semicolon-separated list of hostnames to monitor")
var infoPeriodSecs = flag.Float64("infoPeriodSecs", 30,
	"number of seconds between printing INFO messages")
var checkPeriodSecs = flag.Float64("checkPeriodSecs", 3,
	"number of seconds between checking whether we can connect to other nodes")
var warnPeriodSecs = flag.Float64("warnPeriodSecs", 60,
	"number of seconds after which we'll issue a warning about a long check " +
	"or DNS resolution")

//var usePrintTopology = flag.Bool("usePrintTopology", false, 
// "If true, we should use printTopology to find out the list of hostnames to monitor")

func main() {
	flag.Parse()

	// get host list
	hosts := strings.Split(*hostnames, ";")

	started := 0
	logChan := make(chan string)
	for idx := range(hosts) {
		if hosts[idx] != "" {
			fmt.Printf("starting thread for host '%s'\n", hosts[idx])
			go runCheckThread(logChan, hosts[idx])
			started++
		}
	}
	if started == 0 {
		fmt.Fprintf(os.Stderr, "you must supply a list of hostnames.\n")
		os.Exit(1)
	}
	fmt.Println("started goroutines.")
	for ;; {
		str := <- logChan
		fmt.Print(str)
	}
}
